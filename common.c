#include "map.h"

#include <stdlib.h>

Map *createMap(int size) {
    Map *res = malloc(sizeof(Map));
    if (res == NULL) {
        return NULL;
    }

    res->items = malloc(sizeof(*res->items) * size);
    if (res->items == NULL) {
        free(res);
        return NULL;
    }

    //@ assert \valid(res);
    //@ assert \valid(res+(0..size-1));
    /*@ loop invariant \valid(res);
        loop invariant 0 <= i <= size;
        loop invariant i < res->maxSize
            ==> \valid(res->items + i);
        loop assigns i, res->items[i];
        loop variant size - i;
    */
    for (int i = 0; i < size; ++i) {
        res->items[i].existent = 0;
    }

    res->maxSize = size;
    res->count = 0;

    return res;
}

void deleteMap(Map *map) {
    for (int i = 0; i < map->maxSize; ++i) {
        map->items[i].existent = 0;
    }

    free(map->items);
    free(map);
}

#define sqsum(n) (((n + 1) * n) / 2)

/*@
    requires n > 0;
    requires \valid(v);
    assigns \nothing;
    allocates \nothing;
    frees \nothing;
    ensures v->c == 7 && v->d == 8;
 */
void checkGetting(int n, Value *v) {
    Map maps[n];

    Item items[sqsum(n)];

    for (int i = 0; i < sqsum(n); ++i) {
        items[i].existent = 0;
    }

    for (int i = 0, offset = 0; i < n; ++i, offset += i) {
        maps[i].items = items + offset;
        maps[i].maxSize = i + 1;
        maps[i].count = 0;
    }

    maps[2].items[0].existent = 1;
    maps[2].items[0].key.a = 1;
    maps[2].items[0].key.b = 2;
    maps[2].items[0].value.c = 3;
    maps[2].items[0].value.d = 4;

    maps[2].items[1].existent = 1;
    maps[2].items[1].key.a = 5;
    maps[2].items[1].key.b = 6;
    maps[2].items[1].value.c = 7;
    maps[2].items[1].value.d = 8;

    maps[2].items[2].existent = 1;
    maps[2].items[2].key.a = 9;
    maps[2].items[2].key.b = 10;
    maps[2].items[2].value.c = 11;
    maps[2].items[2].value.d = 12;

    maps[0].items[0].existent = 1;
    Key k;
    k.a = 5;
    k.b = 6;

    getElement(&maps[2], &k, &maps[0].items[0].value);
    maps[0].items[0].key = k;

    getElement(&maps[0], &k, v);
}
