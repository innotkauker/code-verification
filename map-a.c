#include "map.h"

int addElement(Map *map, Key *key, Value *value) {
    if (map->items[0].existent) {
        int copied = 0;
        /*@ loop invariant \valid(map);
            loop invariant \valid(map->items);
            loop invariant 0 <= i <= map->maxSize;
            loop invariant i < map->maxSize
                ==> \valid(map->items + i);
            loop invariant map->maxSize;
            loop assigns i;
            loop assigns map->items[0..map->maxSize-1];
            loop variant map->maxSize - i;
        */
        for (int i = 1; i < map->maxSize && !copied; ++i) {
            if (!map->items[i].existent ||
                    map->items[i].key.a == map->items[0].key.a && map->items[i].key.b == map->items[0].key.b) {
                map->items[i] = map->items[0];
                copied = 1;
            }
        }
        if (!copied) {
            for (int i = 0; i < map->maxSize && !copied; ++i) {
                for (int j = i + 1; j < map->maxSize && !copied; ++j) {
                    if (map->items[i].key.a == map->items[j].key.a && map->items[i].key.b == map->items[j].key.b) {
                        map->items[j] = map->items[0];
                        copied = 1;
                    }
                }
            }
        }
        if (!copied) {
            return 0;
        }
    }
    map->items[0].existent = 1;
    map->items[0].key = *key;
    map->items[0].value = *value;

    return 1;
}

int removeElement(Map *map, Key *key, Value *value) {
    int was = getElement(map, key, value);

    if (was) {
        for (int i = 0; i < map->maxSize; ++i) {
            if (map->items[i].key.a == key->a && map->items[i].key.b == key->b) {
                map->items[i].existent = 0;
            }
        }

        for (int i = 0; i < map->maxSize; ++i) {
            if (!map->items[i].existent) {
                for (int j = i + 1; j < map->maxSize; ++j) {
                    if (map->items[j].existent) {
                        map->items[i] = map->items[j];
                        break;
                    }
                }
            }
        }
    }

    return was;
}

int getElement(Map *map, Key *key, Value *value) {
    /*@
        loop invariant \valid(map);
        loop invariant \valid(value);
        loop invariant \valid(map->items+(0..map->maxSize-1));
        loop invariant 0 <= i <= map->maxSize;
        loop invariant i < map->maxSize
            ==> \valid(map->items + i);
        for present:
            loop invariant \exists integer k;
                i <= k < map->maxSize &&
                    map->items[k].existent &&
                        map->items[k].key.a == key->a &&
                        map->items[k].key.b == key->b;
        loop assigns i, *value;
        loop variant map->maxSize - i;
    */
    for (int i = 0; i < map->maxSize &&
                    (map->items[i].existent || i < map->maxSize - 1 && map->items[i + 1].existent); ++i) {
        if (map->items[i].existent &&
                map->items[i].key.a == key->a &&
                map->items[i].key.b == key->b) {
            *value = map->items[i].value;
            return 1;
        }
    }

    return 0;
}
