#ifndef __MAP_H__
#define __MAP_H__

typedef struct {
   int a;
   int b;
} Key;

typedef struct {
   int c;
   int d;
} Value;

typedef struct {
   Key key __attribute__((noembed));
   Value value;
   int existent;
} Item;

typedef struct {
   Item *items;
   int maxSize;
   int count;
} Map;

/*@
predicate KeysEqual(Key *k0, Key *k1) = k0->a == k1->a && k0->b == k1->b;
predicate ValuesEqual(Value *v0, Value *v1) = v0->c == v1->c && v0->d == v1->d;
predicate ItemsEqual(Item *i0, Item *i1) = KeysEqual(&i0->key, &i1->key) && ValuesEqual(&i0->value, &i1->value);
predicate ItemPresent(Map *m, Key *k, Value *v) = \exists integer i; 0 <= i < m->maxSize
        && m->items[i].existent && KeysEqual(&m->items[i].key, k) && ValuesEqual(&m->items[i].value, v);
predicate KeyPresent(Map *m, Key *k) = \exists integer i; 0 <= i < m->maxSize
        && m->items[i].existent && KeysEqual(&m->items[i].key, k);

predicate KeysEqualThere{L0, L1}(Key *k0, Key *k1) = \at(k0->a, L0) == \at(k1->a, L1) && \at(k0->b, L0) == \at(k1->b, L1);
predicate ValuesEqualThere{L0, L1}(Value *v0, Value *v1) = \at(v0->c, L0) == \at(v1->c, L1) && \at(v0->d, L0) == \at(v1->d, L1);
predicate ItemPresentThere{L0, L1}(Map *m, Key *k, Value *v) = \exists integer i; 0 <= i < \at(m->maxSize, L0)
		&& \at(m->items[i].existent, L0) &&
		KeysEqualThere{L0, L1}(\at(&m->items[i].key, L0), k) &&
		ValuesEqualThere{L0, L1}(\at(&m->items[i].value, L0), v);

logic integer elem_existent(Map *m, integer pos) = m->items[pos].existent;
logic Item *get_item_by_i(Map *m, integer pos) = m->items + pos;
logic Key *get_key(Map *m, integer pos) = &(m->items[pos].key);

predicate MapMemoryIsOk(Map *m) = \valid(m) && \valid(m->items + (0..m->maxSize-1));

predicate ExistencyRuleHolds(Map *m) =
        \forall integer i, j;
                0 < i + 1 < j < m->maxSize && !elem_existent(m, i) && !elem_existent(m, i+1)
                ==> !elem_existent(m, j);

predicate MapHasNoDupes(Map *m) =
        \forall integer i, j;
                0 <= i < j < m->maxSize
                && get_item_by_i(m, i)->existent && get_item_by_i(m, j)->existent
                && KeysEqual(&(get_item_by_i(m, i)->key), &(get_item_by_i(m, j)->key))
                ==> ValuesEqual(&(get_item_by_i(m, i)->value), &(get_item_by_i(m, j)->value));

logic integer get_count(Map *m, integer pos) =
	pos == m->maxSize ? 0 : (m->items[pos].existent ? 1 : 0) + get_count(m, pos + 1);

predicate CountEqual(Map *m) = m->count == get_count(m, 0);

predicate CountCorrect(Map *m) = 0 <= m->count < m->maxSize && CountEqual(m);

predicate MapIsCorrect(Map *m) = MapHasNoDupes(m) && ExistencyRuleHolds(m) && MapMemoryIsOk(m) && CountCorrect(m);

// Here we assume that memory blocks which are addressed by pointers of different types can't intersect
// and also that the distance between any two different pointers in bytes is greater than the size of the object pointed by
// the pointer with the smaller value.
predicate NoMemoryHacks(Map *m, Value *value) =
        \forall integer i; 0 <= i < m->maxSize ==>
                &m->items[i].value != value;
predicate NoMemoryHacks(Map *m, Key *key, Value *value) =
        \forall integer i; 0 <= i < m->maxSize ==>
                &m->items[i].value != value &&
                &m->items[i].key != key;

predicate HasDuplicates(Map *m) =
        \exists integer i, j; 0 <= i < j < m->maxSize &&
                m->items[i].existent && m->items[j].existent &&
                ItemsEqual(&m->items[i], &m->items[j]);

predicate AbleToAdd(Map *m, Key *k) = m->count < m->maxSize || HasDuplicates(m) || KeyPresent(m, k);

predicate NothingNew{L0, L1}(Map *map) =
	\forall integer i; 0 <= i < \at(map->maxSize, L0) &&
		elem_existent{L0}(map, i)
			==> ItemPresentThere{L1, L0}(map, \at(&get_item_by_i(map, i)->key, L0),
							     \at(&get_item_by_i(map, i)->value, L0));

predicate NothingNew{L0, L1}(Map *map, Key *k) =
	\forall integer i; 0 <= i < \at(map->maxSize, L0) &&
		elem_existent{L0}(map, i) && !KeysEqual{L0}(\at(&get_item_by_i(map, i)->key, L0), k)
			==> ItemPresentThere{L1, L0}(map, \at(&get_item_by_i(map, i)->key, L0),
							     \at(&get_item_by_i(map, i)->value, L0));
*/

/*@ requires size > 0;
    assigns \nothing;
    allocates \result;
    allocates \result->items;
    frees \nothing;
    ensures \result == \null
            || \valid(\result)
                && MapIsCorrect(\result)
                //&& \allocation(\result) == \dynamic
                && \result->count == 0 && \result->maxSize == size;
 */
Map *createMap(int size);
void deleteMap(Map *map);

/*@ requires \valid(map);
    requires MapIsCorrect(map);
    requires \valid(key);
    requires \valid(value);
    requires NoMemoryHacks(map, key, value);
    allocates \nothing;
    frees \nothing;
    ensures MapIsCorrect(map);
    assigns map->count;
    assigns map->items[0..map->maxSize-1];
    behavior full:
        assumes !AbleToAdd(map, key);
        ensures NothingNew{Pre, Post}(map);
        ensures NothingNew{Post, Pre}(map);
        ensures \result == 0;
    behavior not_full:
        assumes AbleToAdd(map, key);
        ensures \result == 1;
        ensures ItemPresent(map, key, value);
        ensures NothingNew{Pre, Post}(map, key);
        ensures NothingNew{Post, Pre}(map, key);
    complete behaviors;
    disjoint behaviors;
 */
int addElement(Map *map, Key *key, Value *value);
int removeElement(Map *map, Key *key, Value *value);

/*@ requires \valid(map);
    requires MapIsCorrect(map);
    requires \valid(key);
    requires \valid(value);
    requires NoMemoryHacks(map, value);
    allocates \nothing;
    frees \nothing;
    behavior not_present:
        assumes !KeyPresent(map, key);
        assigns \nothing;
        ensures \result == 0;
    behavior present:
        assumes KeyPresent(map, key);
        assigns *value;
        ensures \result == 1;
        ensures ItemPresent(map, key, value);
    disjoint behaviors;
    complete behaviors;
 */
int getElement(Map *map, Key *key, Value *value);

#endif // __MAP_H__
